CC = gcc
CFLAGS = -std=c89 -pedantic -Wall -Werror -g -D_GNU_SOURCE
RM = rm -fv

.PHONY: all clean

all: shm-client.out shm-server.out
	
shm-common.o: shm-common.c shm-common.h
	$(CC) $(CFLAGS) -c  -o $@ $<
shm-washer.o: shm-washer.c
	$(CC) $(CFLAGS) -c -o $@ $<
shm-client.out: shm-client.c shm-common.o shm-washer.o
	$(CC) $(CFLAGS) -o $@ $^
shm-server.out: shm-server.c shm-common.o shm-washer.o
	$(CC) $(CFLAGS) -o $@ $^
clean:
	$(RM) *.o *.out
