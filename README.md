Le lien du projet: https://elearn.univ-pau.fr/pluginfile.php/763822/mod_resource/content/1/tp1-20210206.pdf

- Les fichiers **shm-washer.c et shm-washer.h** contiennent respectivement les _définitions et les déclarations_ pour la machine à laver.
- Les fichiers **shm-client.c et shm-server.c** contiennent respectivement _les définitions_ pour le client et le serveur.
- Les fichiers **shm-common.c et shm-common.h** contiennent respectivement _les définitions et les déclarations_ pour ce qui n’est pas spécifique au client, au serveur et à la machine à laver.

**Pour le signal d'arret le ^C**
http://www.singaomara.com/repCC++/SignalSystem.html

Important d'avoir ces fichiers la ->
- tp1-roman-badanin-olivier-baconnais/
- tp1-roman-badanin-olivier-baconnais/**file.ftok**
- tp1-roman-badanin-olivier-baconnais/**Makefile**
- tp1-roman-badanin-olivier-baconnais/**shm-client.c**
- tp1-roman-badanin-olivier-baconnais/**shm-common.c**
- tp1-roman-badanin-olivier-baconnais/**shm-common.h**
- tp1-roman-badanin-olivier-baconnais/**shm-server.c**
- tp1-roman-badanin-olivier-baconnais/**shm-washer.c**
- tp1-roman-badanin-olivier-baconnais/**shm-washer.h**

**_Fonction a uilisé uniquement_**

- [x] int fprintf(FILE *stream, const char *format, ...);
- [x] key_t ftok(const char *pathname, int proj_id);
- [ ] int getchar(void);
- [x] int getopt_long(int argc, char * const argv[], const char *optstring, const struct option* longopts, int *longindex);
- [x] struct tm *localtime(const time_t *timep);
- [x] int printf(const char *format, ...);
- [x] void *shmat(int shmid, const void *shmaddr, int shmflg);
- [ ] int shmctl(int shmid, int cmd, struct shmid_ds *buf);
- [x] int shmget(key_t key, size_t size, int shmflg);
- [x] unsigned int sleep(unsigned int seconds);
- [ ] int strcmp(const char *s1, const char *s2);
- [ ] char *strcpy(char *dest, const char *src);
- [ ] size_t strlen(const char *s);
- [x] long int strtol(const char *nptr, char **endptr, int base);
- [x] time_t time(time_t *t);

**_Verification a faire a la fin:_**
- [ ] Supprimer le fichier README.md (car n'est pas dans la liste des fichiers autorisé).
- [ ] Verifier l'orthographe dans tous les fichiers
- [x] Corrigé le USAGE dans server, le nombre de caractere limité dans printf est de 509, dans notre USAGE on a 802. Limite faire 2 USAGE().
- [x] les messages d'erreur doivent être écrit dans le flux d'erreur standard.
- [ ] Traduire les _commentaires_ pour les mettre en anglais.

**_commentaire:_**
fonction getopt_long: la fonction permet de paramétrer les options, avec cette option si tu tapes --help au lieu de -h ça fonctionne quand même.
