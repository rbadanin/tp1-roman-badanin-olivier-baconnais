/**
 * fichier shm-client.c.
*/

#include "shm-common.h" /*Fichier commun à server.c et client.c.*/

/* Nombre maximum d'argument. */
#define MAX_ARGUMENTS 13

int main (int argc, char* argv[]) {

    /*#######################################################################*/
    /*############################déclaration variable#######################*/
    /*#######################################################################*/

    int hv               = 0;               /* variable pour mixité -h -v.              */
    int opt              = -1;              /* variable pour getopt_long.               */
    char* endptr         = NULL;            /* pour lecture arguments.                  */
    int shmid            = -1;              /* ID de mémoire partagée.                  */
    key_t key            = -1;              /* variable pour clé segment partagé.       */
    shm_washer_t* mem    = NULL;            /* identification segment.                  */
    unsigned int sl_time = 1;               /* variable taux de rafraichissement.       */
    unsigned int Proj_ID = 1;               /* variable pour ID shm.                    */
    char* PathName       = "file.ftok";     /* variable pour chemin fichier shm.        */
    int Tries            = 1;               /* variable pour nombre d'essai(s).         */
    shm_washer_t washer;                    /* instanciation objet de type washer_t.    */
    shm_washer_t* wash   = &washer;         /* pointeur sur structure de type washer_t. */

    /* Structure pour décrire les options possibles. */
    static struct option long_options[] = {
        { "help",         no_argument,       0, 'h' },
        { "key-proj-id",  required_argument, 0, 'i' },
        { "key-pathname", required_argument, 0, 'p' },
        { "seconds",      required_argument, 0, 's' },
        { "tries",        required_argument, 0, 't' },
        { "version",      no_argument,       0, 'v' },
        { "washer-brand", required_argument, 0, 'b' },
        { "-washer-model",required_argument, 0, 'm' },
        { 0,              0,                 0,  0  }
    };

    /*#######################################################################*/
    /*###################### démarrage programme ############################*/
    /*#######################################################################*/

    /* Vérification du nombre d'argument(s). */
    if (verif_arg(MAX_ARGUMENTS, argc) != 0)
        return 1;

    /* initialisation de la structure. */
    if(shm_washer_set_brand(wash, "Default brand") == shm_washer_error_brand)
        fprintf(stderr, "./shm-client.out:%s:%d: Unable to set the washer "
                        "brand to \"Default brand\".\n", __FILE__, __LINE__);

    if (shm_washer_set_model(wash, "Default model") == shm_washer_error_model)
        fprintf(stderr, "./shm-client.out:%s:%d: Unable to set the washer "
                        "model to \"Default model\".\n", __FILE__, __LINE__);

    /* Traitement des options. */
    while ((opt = getopt_long(argc, argv, "b:hi:m:p:s:t:v", long_options, NULL)) != -1) {
        switch (opt) {
            case 'b': /* configuration de la marque de la machine. */
                if (shm_washer_set_brand(wash, optarg) == shm_washer_error_brand) {
                    fprintf(stderr, "./shm-client.out:%s:%d: Unable to set the washer "
                                    "brand to \"%s\".\n", __FILE__, __LINE__, optarg);
                    return 1;
                }
                break;
            case 'h': /* affichage de l'aide. */
                HelpVersionCommand(1, "client");
                hv++;
                break;
            case 'i': /* configuration de l'ID segment. */
                Proj_ID = strtol(optarg, &endptr, 10);
                break;
            case 'm': /* configuration du modèle de la machine. */
                if (shm_washer_set_model(wash,optarg) == shm_washer_error_model) {
                    fprintf(stderr, "./shm-client.out:%s:%d: Unable to set the washer "
                                    "model to \"%s\".\n", __FILE__, __LINE__, optarg);
                    return 1;
                }
                break;
            case 'p': /* configuration du chemin pour fichier ftok. */
                PathName = optarg;
                break;
            case 's': /* configuration du taux de rafraichissement. */
                sl_time = strtol(optarg, &endptr, 10);
                break;
            case 't': /* configuration du nombre d'essai(s). */
                Tries = strtol(optarg, &endptr, 10);
                break;
            case 'v': /* affichage de la version. */
                HelpVersionCommand(0, "client");
                hv++;
                break;
            default:
                break;
        }
    };

    /* h||v ou h&&v => exit processus. */
    if (hv != 0)
        return 0;

    /* Affichage de l'ID et du chemin. */
    affichage_USAGE(Proj_ID, PathName);

    /* Création de la clé. */
    key = ftok(PathName, Proj_ID);

    /* Si la clé n'est pas générée elle retourne -1. */
    if (key == -1) {
        fprintf(stderr, "./shm-client.out:%s:%d: Unable to create the System V IPC key from the \"%s\""
                        " pathname and the \"%d\" project identifier.\n", __FILE__, __LINE__, PathName, Proj_ID);
        return 1;
    }

    /* Recherche du segment avec le couple key/Proj_ID. */
    shmid = shmget(key, Proj_ID , IPC_EXCL);

    /* Si le segment n'est pas crée, -1 est retourné. */
    if (shmid == -1) {
        fprintf(stderr,"./shm-client.out:%s:%d: Unable to get the identifier of the System V shared"
                       " memory segment from the \"0x%08x\" key.\n", __FILE__, __LINE__, key);
        return 1;
    }

    /* Liaison du processus au segment. */
    mem = (shm_washer_t*) shmat(shmid, NULL, 0);

    /* Si le pointeur est vide, la liaison est incorrect. */
    if (mem == (void *) -1)
        return 1;

    /*#######################################################################*/
    /*#################### Exploitation du contenu du segment ###############*/
    /*#######################################################################*/

    while (Tries != 0)
    {
        shm_washer_copy(*wash, mem);
        /* Mode intéractif. */
        if (sl_time <= 0) {
            shm_washer_print(*wash);
            printf("Press the Enter key to continue...");
            while (getchar() != '\n');
            if (shm_washer_is_empty(*mem) == 0) {
                fprintf(stderr, "./shm-client.out:%s:%d: Unable to copy the washer because the "
                                "target washer is not empty\n", __FILE__, __LINE__);
                return 1;
            }
        }
        /* Mode non intéractif. */
        else {
            shm_washer_print(*mem);
            sleep(sl_time);
        }

        /* Si nombre d'essai(s) > 0. */
        if (Tries > 0)
            Tries--;
    }

    return 0;
}
