/**
  * fichier shm-common.c.
*/
#include "shm-common.h" /*Fichier commun à server.c et client.c.*/

/* Affichage de l'option help du serveur. */
void affichage_Server() {
    printf("Usage: ./shm-server.out [OPTION]...\n");
    printf("Receive washers from clients through shared memory.\n\n");
    printf("Options:\n");
    printf("%s\n", USAGE_HELP_HEADER);
    printf("%s", USAGE_HELP_FOOTER);
    printf("%s\n", USAGE_FOOTER);
}

/* Affichage de l'option help du client. */
void affichage_Client() {
    printf("Usage: ./shm-client.out [OPTION]...\n");
    printf("Send a washer to a server through shared memory.\n\n");
    printf("Options:\n");
    printf("\t-b, --washer-brand=BRAND\n");
    printf("\t\tset the washer brand to BRAND (the default value is \"Default brand\")\n");
    printf("%s",USAGE_HELP_HEADER);
    printf("\n\t-m, --washer-model=MODEL\n");
    printf("\t\tset the washer model to MODEL (the default value is \"Default model\")\n");
    printf("%s",USAGE_HELP_FOOTER);
    printf("%s\n",USAGE_FOOTER);
}

/* Affichage de l'en-tête présentation. */
void affichage_USAGE(int proj_id, char* pathname) {
    printf("proj_id = \"%d\"\npathname = \"%s\"\n",proj_id, pathname);
}

/* Affichage help ou version (mixte). */
void HelpVersionCommand(int help, char* nom) {
    /* Affiche help. */
    if (help == 1) {
        if (strcmp(nom, "server") == 0)
            affichage_Server();
        else
            affichage_Client();
    }
    /* Affiche version. */
    else {
        if (strcmp(nom, "server") == 0)
            printf("%s\n", USAGE_VERSION);
        else
            printf("%s\n", USAGE_VERSION_CLIENT);
    }
}

/* Vérification du nombre d'argument(s). */
int verif_arg(int nb_arg, int argc) {
    if (argc > nb_arg) {
        fprintf(stderr, "Nombre d\'arguments incorrect\n");
        return 1;
    }
    return 0;
}
