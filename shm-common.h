/**
  * fichier shm-commn.h.
*/

#ifndef SHM_COMMON_H
#define SHM_COMMON_H

#include <stdio.h>      /* bibliotheque standard.                                              */
#include <stdlib.h>     /* bibliotheque standard.                                              */
#include <string.h>     /* bibliothèque standard strcpy,strcmp, etc...                         */
#include <sys/ipc.h>    /* bibliotheque pour création des clés de segment.                     */
#include <sys/shm.h>    /* bibliothèque pour traitement mémoire partager: shm = SHared Memory. */
#include <getopt.h>     /* bibliothèque pour traiment des options.                             */
#include "shm-washer.h" /* Pour paramétrer les arguments du client.                            */


#define USAGE_HELP_HEADER\
    "\t-h, --help\n"\
    "\t\tdisplay this help and exit\n"\
    "\t-i, --key-proj-id=PROJ_ID\n"\
    "\t\tset the key project identifier to PROJ_ID (the default value is \"1\")"\

#define USAGE_HELP_FOOTER \
    "\t-p, --key-pathname=PATHNAME\n"\
    "\t\tset the key pathname to PATHNAME (the default value is \"file.ftok\")\n"\
    "\t-s, --seconds=SECONDS\n"\
    "\t\tset the seconds between each try (the default value is \"1\", a value less than or "\
    "equal to 0 enables the interactive mode where the input stream is read)\n"\
    "\t-t, --tries=TRIES\n"\
    "\t\tset the number of tries to TRIES (the default value is \"-1\", a negative value means\n"\
    "repeat for ever)\n"\
    "\t-v, --version\n"\
    "\t\toutput version information and exit\n\n"

#define USAGE_FOOTER\
    "Report bugs to Olivier Baconnais <o.baconnais@etud.univ-pau.fr> and Roman Badanin <r.badanin@etud.univ-pau.fr>."


#define USAGE_VERSION\
    "shm-server 20210218\n\n"\
    "Copyright (C) 2021 Olivier Baconnais and Roman Badanin.\n\n"\
    "Written by Olivier Baconnais <o.baconnais@etud.univ-pau.fr> and Roman Badanin <o.baconnais@etud.univ-pau.fr>."

#define USAGE_VERSION_CLIENT\
    "shm-client 20210210\n\n"\
    "Copyright (C) 2021 Olivier Baconnais and Roman Badanin.\n\n"\
    "Written by Olivier Baconnais <o.baconnais@etud.univ-pau.fr> and Roman Badanin <r.badanin@etud.univ-pau.fr>."

/**
 * Affichage de l'option version/help.
*/
void HelpVersionCommand(int, char*);

/**
 * Affichage de l'option version (mixte).
*/
void affichage_USAGE(int, char*);

/**
 * Affichage de l'option help du client.
*/
void affichage_Server();

/**
 * Affichage de l'option help du serveur.
*/
void affichage_Client();

/**
 * Vérification du nombre d'argument.
*/
int verif_arg(int, int);

/**
 * Traitement de la date.
*/
void set_date(time_t, int, int, int, struct tm*);

#endif
