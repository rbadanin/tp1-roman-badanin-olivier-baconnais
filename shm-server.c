/**
  * fichier shm-server.c.
*/
#include "shm-common.h" /*Fichier commun à server.c et client.c.*/

/* Nombre maximum d'arguments */
#define MAX_ARGUMENTS 11

int main (int argc, char* argv []) {

    /*#######################################################################*/
    /*########################### Déclaration variable ######################*/
    /*#######################################################################*/

    int hv               = 0;           /* Variable pour mixiter -h -v.            */
    int opt              = -1;          /* Variable pour getopt_long.              */
    char* endptr         = NULL;        /* Pour lecture optarg.                    */
    int shmid            = -1;          /* ID de mémoire partagée.                 */
    key_t key            = -1;          /* Variable pour clé segment partagé.      */
    shm_washer_t* mem    = NULL;        /* Adressage segment.                      */
    unsigned int sl_time = 1;           /* Variable taux de rafraichissement.      */
    unsigned int Proj_ID = 1;           /* Variable pour ID shm.                   */
    char* PathName       = "file.ftok"; /* Variable pour chemin fichier shm.       */
    int Tries            = -1;          /* Variable pour le nombre d'essai.        */

    /* Structure pour décrire les options possibles. */
    static struct option long_options[] = {
        { "help",         no_argument,       0, 'h' },
        { "key-proj-id",  required_argument, 0, 'i' },
        { "key-pathname", required_argument, 0, 'p' },
        { "seconds",      required_argument, 0, 's' },
        { "tries",        required_argument, 0, 't' },
        { "version",      no_argument,       0, 'v' },
        { 0,              0,                 0,  0  }
    };

    /*#######################################################################*/
    /*###################### Démarrage programme ############################*/
    /*#######################################################################*/

    /* Vérification du nombre d'argument. */
    if (verif_arg(MAX_ARGUMENTS, argc) != 0)
        return 1;

    /* Traitement des options. */
    while ((opt = getopt_long(argc, argv, "hi:p:s:t:v", long_options, NULL)) != -1) {
        switch (opt) {
            case 'h': /* affichage de l'aide. */
                HelpVersionCommand(1, "server");
                hv++;
                break;
            case 'i': /* configuration de l'ID segment. */
                Proj_ID = strtol(optarg, &endptr, 10);
                break;
            case 'p': /* configuration du chemin pour fichier ftok. */
                PathName = optarg;
                break;
            case 's': /* configuration du taux de rafraichissement. */
                sl_time = strtol(optarg, &endptr, 10);
                break;
            case 't': /* configuration du nombre d'essai(s). */
                Tries = strtol(optarg, &endptr, 10);
                break;
            case 'v': /* affichage de la version. */
                HelpVersionCommand(0, "server");
                hv++;
                break;
            default:
                break;
        }
    };

    /* h||v ou h&&v => exit processus. */
    if (hv != 0)
        return 0;

    /* Affichage de l'ID et du chemin. */
    affichage_USAGE(Proj_ID, PathName);

    /*#######################################################################*/
    /*############### Création du segment de mémoire partagé ################*/
    /*#######################################################################*/

    /* Génération de la clé du segment. */
    key = ftok(PathName, Proj_ID);

    /* Si la clé n'est pas générée elle retourne -1. */
    if (key == -1) {
        fprintf(stderr, "./shm-server.out:%s:%d: Unable to create the System V IPC key from the \"%s\""
                        " pathname and the \"%d\" project identifier.\n", __FILE__, __LINE__, PathName, Proj_ID);
        return 1;
    }

    /* Création du segment partagé. */
    shmid = shmget(key, Proj_ID, IPC_CREAT | 0600 | IPC_EXCL);

    /* Si le segment n'est pas crée ou si il existe déjà la fonction shmget() retourne -1. */
    if (shmid == -1) {
        fprintf(stderr,"./shm-server.out:%s:%d: Unable to get the identifier of the System V shared"
                       " memory segment from the \"0x%08x\" key.\n", __FILE__, __LINE__, key);
        return 1;
    }

    /* Liaison du processus au segment. */
    mem = (shm_washer_t*) shmat(shmid, NULL, 0);

    /* Si le pointeur est vide, la liaison est incorrect. */
    if (mem == (void *) -1)
        return 1;

    /*#######################################################################*/
    /*######################## Suppression du segment #######################*/
    /*#######################################################################*/

    while (Tries != 0) {
        /* Mode intéractif. */
        if (sl_time <= 0) {
            printf("Press the Enter key to continue...");
            while (getchar() != '\n');
            if (shm_washer_is_empty(*mem) == 0) {
                shm_washer_print(*mem);
                shm_washer_empty(mem);
            }
        }
        /* Mode non intéractif. */
        else {
            if (shm_washer_is_empty(*mem) == 0) {
                shm_washer_print(*mem);
                shm_washer_empty(mem);
            }
            sleep(sl_time);
        }

        /* Si nombre d'essai(s) > 0. */
        if (Tries > 0)
            Tries--;
    }

    if (shmctl(shmid, IPC_RMID, NULL) == -1)
        return 1;
    return 0;
}
