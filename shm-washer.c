/**
  * fichier shm-washer.c.
*/
#include "shm-washer.h"

/**
 * Copies a source washer to a target washer.
 * \param source The source washer.
 * \param target The target washer.
 * \return shm_washer_error_not_empty if the target washer is not empty, shm_washer_error_brand if
the shm_washer_set_brand function returns shm_washer_error_brand, shm_washer_error_model if the
shm_washer_set_model function returns shm_washer_error_model, else 0.
*/
int shm_washer_copy(shm_washer_t source, shm_washer_t *target) {
    if (shm_washer_is_empty(*target) == shm_washer_error_not_empty)
        return shm_washer_error_not_empty;
    else if (shm_washer_set_brand(target, source.brand) == shm_washer_error_brand)
        return shm_washer_error_brand;
    else if (shm_washer_set_model(target, source.model) == shm_washer_error_model)
        return shm_washer_error_model;
    return 0;
}

/**
 * Empties a washer (i.e., sets the washer brand and model to an empty string).
 * \param washer The washer.
 */
void shm_washer_empty(shm_washer_t *washer) {
    strcpy(washer->brand, "");
    strcpy(washer->model, "");
}

/**
 * Checks if a washer is empty (i.e., checks if the washer brand and model are an empty string).
 * \param washer The washer.
 * \return 1 if the washer is empty, else 0.
 */
int shm_washer_is_empty(shm_washer_t washer) {
    return (strlen(washer.brand) == 0 && strlen(washer.model) == 0) ? 1 : 0;
}

/**
 * Prints a washer using the "YYYY-MM-DD HH:MM:SS: brand: model" format.
 * \param washer The washer.
 */
void shm_washer_print(shm_washer_t washer) {
    time_t now;
    int year, month, day, hour, min, sec;
    struct tm* h_local = NULL;
    time(&now);
    h_local = localtime(&now);
    year    = h_local->tm_year + 1900;
    month   = h_local->tm_mon + 1;
    day     = h_local->tm_mday;
    hour    = h_local->tm_hour;
    min     = h_local->tm_min;
    sec     = h_local->tm_sec;
    printf("%02d-%02d-%02d %02d:%02d:%02d: %s: %s\n", year, month, day, hour, min, sec, washer.brand, washer.model);
}

/**
 * Sets the brand of a washer.
 * \param washer The washer.
 * \param brand The brand.
 * \return shm_washer_error_brand if the washer brand length is greater than SHM_WASHER_BRAND_SIZE - 1, else 0.
*/
int shm_washer_set_brand(shm_washer_t *washer, const char *brand) {
    if (strlen(brand) > SHM_WASHER_BRAND_SIZE - 1)
        return shm_washer_error_brand;
    strcpy(washer->brand, brand);
    return 0;
}

/**
 * Sets the model of a washer.
 * \param washer The washer.
 * \param model The model.
 * \return shm_washer_error_model if the washer model length is greater than SHM_WASHER_MODEL_SIZE
- 1, else 0.
*/
int shm_washer_set_model(shm_washer_t *washer, const char *model) {
    if (strlen(model) > SHM_WASHER_MODEL_SIZE - 1)
        return shm_washer_error_model;
    strcpy(washer->model, model);
    return 0;
}
