/**
 * file shm-washer.h.
*/
#ifndef SHM_WASHER_H
#include <stdio.h>   /* bibliotheque standard.                                              */
#include <stdlib.h>  /* bibliotheque standard.                                              */
#include <string.h>  /* bibliotheque standard.                                              */
#include <time.h>    /* bibliothèque pour traitement de l'heure.                            */
#define SHM_WASHER_H
/**
 * The size of the washer brand.
*/
#define SHM_WASHER_BRAND_SIZE 15
/**
 * The size of the washer model.
*/
#define SHM_WASHER_MODEL_SIZE 20
/**
* A washer.
*/
typedef struct
{
    char brand[SHM_WASHER_BRAND_SIZE]; /**< The washer brand (example: "Whirlpool"). */
    char model[SHM_WASHER_MODEL_SIZE]; /**< The washer model (example: "FSCR-10427"). */
} shm_washer_t;
/**
 * A washer error.
*/
typedef enum {
    shm_washer_error_brand = -3,
    shm_washer_error_not_empty,
    shm_washer_error_model
} shm_washer_error_t;
/**
 * Copies a source washer to a target washer.
 * \param source The source washer.
 * \param target The target washer.
 * \return shm_washer_error_not_empty if the target washer is not empty, shm_washer_error_brand if
the shm_washer_set_brand function returns shm_washer_error_brand, shm_washer_error_model if the
shm_washer_set_model function returns shm_washer_error_model, else 0.
*/
int shm_washer_copy(shm_washer_t source, shm_washer_t *target);
/**
 * Empties a washer (i.e., sets the washer brand and model to an empty string).
 * \param washer The washer.
 */
void shm_washer_empty(shm_washer_t *washer);
/**
 * Checks if a washer is empty (i.e., checks if the washer brand and model are an empty string).
 * \param washer The washer.
 * \return 1 if the washer is empty, else 0.
 */
int shm_washer_is_empty(shm_washer_t washer);
/**
 * Prints a washer using the "YYYY-MM-DD HH:MM:SS: brand: model" format.
 * \param washer The washer.
 */
void shm_washer_print(shm_washer_t washer);
/**
 * Sets the brand of a washer.
 * \param washer The washer.
 * \param brand The brand.
 * \return shm_washer_error_brand if the washer brand length is greater than SHM_WASHER_BRAND_SIZE
- 1, else 0. */
int shm_washer_set_brand(shm_washer_t *washer, const char *brand);
/**
 * Sets the model of a washer.
 * \param washer The washer.
 * \param model The model.
 * \return shm_washer_error_model if the washer model length is greater than SHM_WASHER_MODEL_SIZE
- 1, else 0. */
int shm_washer_set_model(shm_washer_t *washer, const char *model);
#endif
